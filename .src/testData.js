/** @format */

export const testData = {
  user_id: "46c9c0e0-5dcd-49b4-8bd1-9611eec4e1aa",
  first_name: "John",
  last_name: "Doe",
  username: "john.p.doe",
  middle_name: "Paul",
  date_created: "Sat Oct 10 2020 20:06:23 GMT-0400 (Eastern Daylight Time)",
  date_modified: null,
  email: "john.p.doe@example.com",
  is_super_user: false,
  is_staff: false,
  account_id: null,
  current_balance: 0,
  account_debits: [100, 300],
  account_credits: [100, 200],
  normal_balance: "debit",
  block_credits: false,
  block_debits: false,
  caution_text: [],
  date_opened: "Sat Oct 10 2020 20:06:23 GMT-0400 (Eastern Daylight Time)",
  account_id: "121678dfhadfa",
};
