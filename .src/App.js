/** @format */

import logo from "./logo.svg";
import "./App.css";
import Bond from "./components/bonds/Bonds";
function App() {
  return (
    <div className="App">
      <Bond />
    </div>
  );
}

export default App;
