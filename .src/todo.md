<!-- @format -->

<!-- https://standardjs.com/#what-you-might-do-if-youre-clever -->
<!-- https://dev.to/ohmyzsh/oh-my-zsh-2020-year-in-review-3729 -->
<!-- https://dev.to/ridhikgovind/learn-react-context-by-building-a-dead-simple-wallet-app-for-beginners-20a5 -->
<!-- https://dev.to/stackfindover/neon-light-button-1fd5     -->
<!-- https://blog.usejournal.com/using-mongodb-as-realtime-db-with-nodejs-c6f52c266750 -->
<!-- https://dev.to/ebereplenty/react-authentication-login-h3i -->
<!-- https://daily.dev/posts/creating-a-killer-github-profile-readme-part-1 -->
<!-- https://codepen.io/ailyntang/full/oJeLdr -->

1. Allow a person to setup thier account.

- is it business account or personal account
- continuous calculation of financial ratios can approve or deny ability to extend credit to customers or advise on gaining new lines of credit.
  {
  question: "adopting a child and don't yet have a social security number for the child",
  form:"irs_form_w_7_a",
  }

I am adopting a child and don't yet have a social security number for the child. How may I claim the child as my dependent?
My spouse and I have provided a home for my niece and her son for the past seven months. She has no income and we provided all of her support during the year. Can I claim both her and her son as dependents?
Is there an age limit on claiming my child as a dependent?
We’re the divorced or legally separated parents of one child. May each parent claim the child as a dependent for a different part of the tax year?
My spouse and I are filing as married filing separately. We both contributed to the support of our son. Can we both claim him as a dependent on our separate returns?
My husband and I were separated the last 11 months of the year and our two minor children lived with me for a greater part of the year than they lived with my husband. My husband provided all the financial support. Who may claim the children as dependents on the tax return?
Are child support payments deductible by the payer and may the payer claim the child as a dependent?
Can a state court determine who may claim a child as a dependent on a federal income tax return?
My daughter was born on December 31. May I claim her as a dependent and also claim the child tax credit?
My daughter was born at the end of the year. We're still waiting for a social security number. May I file my return now and provide her social security number later?
My child was stillborn. May I claim my child as a dependent on my tax return?
