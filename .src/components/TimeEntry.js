/** @format */

export default function TimeEntry() {
  return (
    <form>
      <input type="time" name="clock_in" />
      <input name="clock_out" />
    </form>
  );
}
