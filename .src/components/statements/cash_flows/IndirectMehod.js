/** @format */

const satements = {
  income_statement: ["statement_of_earnings", "profit_and_loss_statement"],
  balance_sheet: [
    "statement_of_financial_position",
    "statement_of_financial_performance",
  ],
  statement_of_owners_equity: "statement_of_retained_earnings",
  "": "",
};

class IndirectMethod {
  constructor() {
    this.CollectionsFromCustomers =
      net_sales_revenue + excess_sales_collections;
    this.supplierPayments = cost_of_goods_sold + advertising_expense;
    this.employeePayments = salaries_expense + excess_expense_payments;
    this.plantAssetAcquisition;
    this.saleOfLand;
  }
}

const test = {
  supplierPayments: 121_000,
};
