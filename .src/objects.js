/** @format */

export const data = [
  {
    account_number: 1,
    classification: "asset",
    account_sub_type: "accounts_receivables",
    active: true,
    sub_account: false,
    current_balance: 200,
  },
];
