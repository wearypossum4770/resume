/** @format */

export default {
  findEmployee: jest.fn(() => Promise.resolve({ data: [] })),
  get: jest.fn(() => Promise.resolve({ data: [] })),
};
