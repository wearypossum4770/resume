/** @format */

export default class Wage {
  constructor() {
    this.that = [
      "HolidayPay",
      "RegularEarnings",
      "OtherGrossEarnings",
      "WithholdingIncome",
      "DiscretionaryPayments",
    ];
  }
}
