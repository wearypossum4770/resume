/** @format */

export default class NonCashActivities {
  constructor(accounts = [{}]) {
    //  Takes an array of account objects
    /**The three sections of the statement of cash flows report only activities that involve cash.
     * Companies make investments that do not require cash. They also obtain financing other than
     * cash. Such transactions are called​ non-cash investing and financing activities. Examples of
     * these activities include the purchase of equipment financed by a​ long-term notes payable or
     * the contribution of equipment by a stockholder in exchange for common stock. These activities
     *  are not included in the statement of cash flows.​ Instead, they appear either as a separate
     * schedule at the bottom of the statement or in the notes to the financial statements. */
  }
  // Acquisition of Plant Assets by Issuing Note Payable
}
